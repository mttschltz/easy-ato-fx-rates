const dateKey = date => date.format('YYYY-MM-DD');

module.exports = {
  dateKey
};
